﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using PropertyChanged;

namespace EjemploWpf.AgregarPrecio
{
    [ImplementPropertyChanged]
    public  class AgregaPrecioViewModel
    {
        public AgregaPrecioViewModel()
        {
            MessageBox.Show("Abriendo agrega precio... Aqui podriamos consultar el precio del dia de hoy");
        }

        public Window ventana;

        public decimal Precio { get; set; }
        public DateTime Fecha { get; set; }

        public ICommand AgregarPrecio => new DelegateCommand(() =>
       {
           //Hacer todo lo necesario para agregar el precio a la base de datos aqui
      
           //using(var c = db())
           //{
           MessageBox.Show("Agregando precio... " + Precio + " el dia de " + Fecha);
           //}
           ventana.Close();
       });

    }

}
