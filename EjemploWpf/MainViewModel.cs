﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using EjemploWpf.AgregarPrecio;
using PropertyChanged;

namespace EjemploWpf
{
    [ImplementPropertyChanged]
    public class MainViewModel
    {
        public string Nombre
        {
            get;set;
        }

        public string Saludo { get; set; }

        public ICommand Aceptar => new DelegateCommand(() =>
        {
            MessageBox.Show("Realizar saludo");
            //Esto es un metodo
            Saludo = "Hola " + Nombre;

            /*
             using(var c = db())
             {
                AgregarPrecio(c, parametros... );
             }
             */
        });

        public ICommand AgregarPrecio => new DelegateCommand(() =>
        {
            var view = new AgregaPrecioView();
            var vm = new AgregaPrecioViewModel();
            vm.ventana = view;
            view.DataContext = vm;

            view.ShowDialog();
        });
    }
}
